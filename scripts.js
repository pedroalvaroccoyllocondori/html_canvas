
// dibujando curvas con canvas


//capturanado canvas
const canvas = document.getElementById('canvas')
const crearGrilla= document.getElementById('boton_grilla');//capturando el boton


const contexto = canvas.getContext('2d')//definir el contexto

crearGrilla.addEventListener('click',()=>{
    distanciaX=20
    distanciaY=20
    colorLinea='blue'
    anchoLinea=1
    for(let i=distanciaX;i<contexto.canvas.width;i+=distanciaX){
        contexto.beginPath()
        contexto.moveTo(i,0)
        contexto.lineTo(i,contexto.canvas.height)
        contexto.strokeStyle=colorLinea
        contexto.lineWidth=anchoLinea
        contexto.stroke()

    }
    for(let i=distanciaY;i<contexto.canvas.height;i+=distanciaY){
        contexto.beginPath()
        contexto.moveTo(0,i)
        contexto.lineTo(contexto.canvas.height,i)
        contexto.strokeStyle=colorLinea
        contexto.lineWidth=anchoLinea
        contexto.stroke()

    }


})